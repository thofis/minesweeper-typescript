/**
 * Created by tom on 5/3/15.
 */
/// <reference path="./view.ts"/>
/// <reference path="./controller.ts"/>
/// <reference path="./typings/jquery/jquery.d.ts"/>
var minesweeper = org.bitbucket.thofis.minesweeper_typescript;
$(document).ready(function () {
    console.log("I'm ready");
    // disable context menu for board
    $('#board').bind('contextmenu', function () {
        return false;
    });
    // default values
    var rows = 9;
    var cols = 9;
    var mines = 10;
    $('#cols').val('' + cols);
    $('#rows').val('' + rows);
    $('#mines').val('' + mines);
    // setup initial controller
    var board = new minesweeper.Controller(cols, rows, mines, function (dummy) {
    });
    // setup inital view
    var view = new minesweeper.View('#board', board);
    view.removeAllEventHandlers();
    // handle start/stop button
    $('#startstop').val('Start');
    $('#startstop').click(function (event) {
        var button = event.target;
        if ($(button).val() == 'Start') {
            // new game started
            $(button).val('Stop');
            board = new minesweeper.Controller($('#cols').val(), $('#rows').val(), $('#mines').val(), function (gameWon) {
                if (gameWon) {
                    alert('Game won!');
                }
                else {
                    alert('Game lost!');
                }
                gameOver(button);
            });
            view = new minesweeper.View('#board', board);
        }
        else {
            // game stopped
            gameOver(button);
        }
    });
    function gameOver(button) {
        $(button).val('Start');
        board.stopGame();
        view.updateBoard();
    }
});
