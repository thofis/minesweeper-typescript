/**
 * Created by tom on 5/3/15.
 */
/// <reference path="./typings/jquery/jquery.d.ts"/>
/// <reference path="./controller.ts"/>
var org;
(function (org) {
    var bitbucket;
    (function (bitbucket) {
        var thofis;
        (function (thofis) {
            var minesweeper_typescript;
            (function (minesweeper_typescript) {
                var View = (function () {
                    function View(parentElementId, board) {
                        this.board = board;
                        this.parent = $(parentElementId).get(0);
                        this.parentId = parentElementId;
                        this.createBoard();
                    }
                    View.prototype.stopGame = function () {
                        this.updateBoard();
                        this.removeAllEventHandlers();
                    };
                    View.prototype.removeAllEventHandlers = function () {
                        $(this.parentId + ' *').off();
                    };
                    View.prototype.createBoard = function () {
                        $(this.parent).empty();
                        var table = $('<table cellpadding="0" cellspacing="0"></table>');
                        for (var r = 0; r < this.board.getNrOfRows(); r++) {
                            var row = $('<tr></tr>');
                            for (var c = 0; c < this.board.getNrOfCols(); c++) {
                                var col = $('<td class="cell"></td>');
                                col.append(this.createImg(c, r));
                                row.append(col);
                            }
                            table.append(row);
                        }
                        $(this.parent).append(table);
                    };
                    View.prototype.createImageId = function (col, row) {
                        return 'c' + col + 'r' + row;
                    };
                    View.prototype.updateBoard = function () {
                        for (var r = 0; r < this.board.getNrOfRows(); r++) {
                            for (var c = 0; c < this.board.getNrOfCols(); c++) {
                                var img = $('#' + this.createImageId(c, r));
                                this.setImageSource(this.board.getCell(c, r), img);
                            }
                        }
                    };
                    View.prototype.createImg = function (col, row) {
                        var _this = this;
                        var img = $('<img id=' + this.createImageId(col, row) + ' class="cell">');
                        var cell = this.board.getCell(col, row);
                        this.setImageSource(cell, img);
                        $(img).click(function () {
                            _this.board.click(col, row);
                            _this.updateBoard();
                        });
                        // handler for right click => flag
                        $(img).mousedown(function (e) {
                            if (e.which == 3) {
                                _this.board.toggleFlag(col, row);
                                _this.updateBoard();
                            }
                        });
                        return img;
                    };
                    View.prototype.setImageSource = function (cell, img) {
                        var imageName = '';
                        //console.log(cell.getMineState().toString())
                        if (cell.getVisibility() == 0 /* HIDDEN */) {
                            if (cell.getFlagState() == 1 /* FLAGGED */) {
                                imageName = 'flag';
                            }
                            else {
                                imageName = 'hidden';
                            }
                        }
                        else {
                            switch (cell.getMineState()) {
                                case 0 /* ONE_NEIGHBOR */:
                                    imageName = '1';
                                    break;
                                case 1 /* TWO_NEIGHBORS */:
                                    imageName = '2';
                                    break;
                                case 2 /* THREE_NEIGHBORS */:
                                    imageName = '3';
                                    break;
                                case 3 /* FOUR_NEIGHBORS */:
                                    imageName = '4';
                                    break;
                                case 4 /* FIVE_NEIGHBORS */:
                                    imageName = '5';
                                    break;
                                case 5 /* SIX_NEIGHBORS */:
                                    imageName = '6';
                                    break;
                                case 6 /* SEVEN_NEIGHBORS */:
                                    imageName = '7';
                                    break;
                                case 7 /* EIGHT_NEIGHBORS */:
                                    imageName = '8';
                                    break;
                                case 9 /* MINE */:
                                    imageName = 'mine';
                                    break;
                                default:
                                    imageName = 'empty';
                                    break;
                            }
                        }
                        img.attr('src', 'images/' + imageName + '.png');
                    };
                    return View;
                })();
                minesweeper_typescript.View = View;
            })(minesweeper_typescript = thofis.minesweeper_typescript || (thofis.minesweeper_typescript = {}));
        })(thofis = bitbucket.thofis || (bitbucket.thofis = {}));
    })(bitbucket = org.bitbucket || (org.bitbucket = {}));
})(org || (org = {}));
