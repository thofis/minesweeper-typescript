/**
 * Created by tom on 5/3/15.
 */
var org;
(function (org) {
    var bitbucket;
    (function (bitbucket) {
        var thofis;
        (function (thofis) {
            var minesweeper_typescript;
            (function (minesweeper_typescript) {
                (function (MineState) {
                    MineState[MineState["ONE_NEIGHBOR"] = 0] = "ONE_NEIGHBOR";
                    MineState[MineState["TWO_NEIGHBORS"] = 1] = "TWO_NEIGHBORS";
                    MineState[MineState["THREE_NEIGHBORS"] = 2] = "THREE_NEIGHBORS";
                    MineState[MineState["FOUR_NEIGHBORS"] = 3] = "FOUR_NEIGHBORS";
                    MineState[MineState["FIVE_NEIGHBORS"] = 4] = "FIVE_NEIGHBORS";
                    MineState[MineState["SIX_NEIGHBORS"] = 5] = "SIX_NEIGHBORS";
                    MineState[MineState["SEVEN_NEIGHBORS"] = 6] = "SEVEN_NEIGHBORS";
                    MineState[MineState["EIGHT_NEIGHBORS"] = 7] = "EIGHT_NEIGHBORS";
                    MineState[MineState["EMPTY"] = 8] = "EMPTY";
                    MineState[MineState["MINE"] = 9] = "MINE";
                })(minesweeper_typescript.MineState || (minesweeper_typescript.MineState = {}));
                var MineState = minesweeper_typescript.MineState;
                (function (Visibility) {
                    Visibility[Visibility["HIDDEN"] = 0] = "HIDDEN";
                    Visibility[Visibility["VISIBLE"] = 1] = "VISIBLE";
                })(minesweeper_typescript.Visibility || (minesweeper_typescript.Visibility = {}));
                var Visibility = minesweeper_typescript.Visibility;
                (function (FlagState) {
                    FlagState[FlagState["NO_FLAG"] = 0] = "NO_FLAG";
                    FlagState[FlagState["FLAGGED"] = 1] = "FLAGGED";
                })(minesweeper_typescript.FlagState || (minesweeper_typescript.FlagState = {}));
                var FlagState = minesweeper_typescript.FlagState;
                var Controller = (function () {
                    function Controller(cols, rows, mines, gameWonOrLostCallback) {
                        this.cols = cols;
                        this.rows = rows;
                        this.mines = mines;
                        this.gameWonOrLostCallback = gameWonOrLostCallback;
                        this.initBoard();
                    }
                    Controller.prototype.getCell = function (col, row) {
                        return this.board[row][col];
                    };
                    Controller.prototype.getNrOfRows = function () {
                        return this.rows;
                    };
                    Controller.prototype.getNrOfCols = function () {
                        return this.cols;
                    };
                    Controller.prototype.click = function (col, row) {
                        //TODO click handling
                        this.board[row][col].setVisibility(1 /* VISIBLE */);
                        if (this.board[row][col].getMineState() == 9 /* MINE */) {
                            console.log("Game Over");
                            this.gameWonOrLostCallback(false);
                        }
                        else {
                            if (this.checkForWin() == true) {
                                this.gameWonOrLostCallback(true);
                            }
                            this.updateNeighborVisibility(col, row);
                        }
                    };
                    Controller.prototype.stopGame = function () {
                        for (var r = 0; r < this.rows; r++) {
                            for (var c = 0; c < this.cols; c++) {
                                this.board[r][c].setVisibility(1 /* VISIBLE */);
                            }
                        }
                    };
                    Controller.prototype.toggleFlag = function (col, row) {
                        this.board[row][col].toggleFlagState();
                        if (this.board[row][col].getFlagState() == 1 /* FLAGGED */) {
                            if (this.checkForWin()) {
                                return this.gameWonOrLostCallback(true);
                            }
                        }
                    };
                    Controller.prototype.checkForWin = function () {
                        // won if, all and only mine fields are flagged
                        var flaggedMines = 0;
                        var flaggedCells = 0;
                        for (var r = 0; r < this.rows; r++) {
                            for (var c = 0; c < this.cols; c++) {
                                var currentCell = this.board[r][c];
                                if (currentCell.getFlagState() == 1 /* FLAGGED */) {
                                    flaggedCells++;
                                    if (currentCell.getMineState() == 9 /* MINE */) {
                                        flaggedMines++;
                                    }
                                }
                            }
                        }
                        return flaggedCells == flaggedMines && flaggedMines == this.mines;
                    };
                    Controller.prototype.initBoard = function () {
                        // place mines randomly
                        var minePlaces = [];
                        for (var i = 0; i < this.cols * this.rows; i++) {
                            minePlaces.push(i);
                        }
                        minePlaces = this.shuffleArray(minePlaces);
                        minePlaces = minePlaces.slice(0, this.mines);
                        for (var i = 0; i < minePlaces.length; i++) {
                            console.log(minePlaces[i]);
                        }
                        // setup board
                        this.board = [];
                        for (var r = 0; r < this.rows; r++) {
                            this.board[r] = [];
                            for (var c = 0; c < this.cols; c++) {
                                var mineState = 8 /* EMPTY */;
                                if (this.createMine(c, r, minePlaces)) {
                                    mineState = 9 /* MINE */;
                                }
                                this.board[r][c] = new Cell(mineState, 0 /* HIDDEN */, 0 /* NO_FLAG */);
                            }
                        }
                        for (var r = 0; r < this.rows; r++) {
                            for (var c = 0; c < this.cols; c++) {
                                if (this.board[r][c].getMineState() == 8 /* EMPTY */) {
                                    var neighbors = this.countNeighbors(c, r);
                                    var newMineState;
                                    switch (neighbors) {
                                        case 1:
                                            newMineState = 0 /* ONE_NEIGHBOR */;
                                            break;
                                        case 2:
                                            newMineState = 1 /* TWO_NEIGHBORS */;
                                            break;
                                        case 3:
                                            newMineState = 2 /* THREE_NEIGHBORS */;
                                            break;
                                        case 4:
                                            newMineState = 3 /* FOUR_NEIGHBORS */;
                                            break;
                                        case 5:
                                            newMineState = 4 /* FIVE_NEIGHBORS */;
                                            break;
                                        case 6:
                                            newMineState = 5 /* SIX_NEIGHBORS */;
                                            break;
                                        case 7:
                                            newMineState = 6 /* SEVEN_NEIGHBORS */;
                                            break;
                                        case 8:
                                            newMineState = 7 /* EIGHT_NEIGHBORS */;
                                            break;
                                        default:
                                            newMineState = 8 /* EMPTY */;
                                    }
                                    this.board[r][c].setMineState(newMineState);
                                }
                            }
                        }
                    };
                    Controller.prototype.updateNeighborVisibility = function (col, row) {
                        for (var x = -1; x <= 1; x++) {
                            for (var y = -1; y <= 1; y++) {
                                // check relevant neighbor cells
                                if (!(x == 0 && y == 0) && this.board[x + row] && this.board[x + row][y + col] && this.board[x + row][y + col].getVisibility() == 0 /* HIDDEN */) {
                                    var currentNeighbor = this.board[x + row][y + col];
                                    if (currentNeighbor.getMineState() != 9 /* MINE */) {
                                        currentNeighbor.setVisibility(1 /* VISIBLE */);
                                        if (currentNeighbor.getMineState() == 8 /* EMPTY */) {
                                            this.updateNeighborVisibility(y + col, x + row);
                                        }
                                    }
                                }
                            }
                        }
                    };
                    Controller.prototype.countNeighbors = function (col, row) {
                        var neighbors = 0;
                        for (var x = -1; x <= 1; x++) {
                            for (var y = -1; y <= 1; y++) {
                                if (this.board[x + row] && this.board[x + row][y + col] && this.board[x + row][y + col].getMineState() == 9 /* MINE */) {
                                    neighbors++;
                                }
                            }
                        }
                        return neighbors;
                    };
                    Controller.prototype.shuffleArray = function (o) {
                        for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x)
                            ;
                        return o;
                    };
                    Controller.prototype.createMine = function (col, row, minePlaces) {
                        for (var i = 0; i < minePlaces.length; i++) {
                            if (minePlaces[i] == col + row * this.cols) {
                                return true;
                            }
                        }
                        return false;
                    };
                    return Controller;
                })();
                minesweeper_typescript.Controller = Controller;
                var Cell = (function () {
                    function Cell(mineState, visibility, flagState) {
                        this.mineState = mineState;
                        this.visibility = visibility;
                        this.flagState = flagState;
                    }
                    Cell.prototype.setMineState = function (mineState) {
                        this.mineState = mineState;
                    };
                    Cell.prototype.getMineState = function () {
                        return this.mineState;
                    };
                    Cell.prototype.getVisibility = function () {
                        return this.visibility;
                    };
                    Cell.prototype.setVisibility = function (visibility) {
                        this.visibility = visibility;
                    };
                    Cell.prototype.getFlagState = function () {
                        return this.flagState;
                    };
                    Cell.prototype.setFlagState = function (flagState) {
                        this.flagState = flagState;
                    };
                    Cell.prototype.toggleFlagState = function () {
                        if (this.flagState == 1 /* FLAGGED */) {
                            this.flagState = 0 /* NO_FLAG */;
                        }
                        else {
                            this.flagState = 1 /* FLAGGED */;
                        }
                    };
                    return Cell;
                })();
            })(minesweeper_typescript = thofis.minesweeper_typescript || (thofis.minesweeper_typescript = {}));
        })(thofis = bitbucket.thofis || (bitbucket.thofis = {}));
    })(bitbucket = org.bitbucket || (org.bitbucket = {}));
})(org || (org = {}));
