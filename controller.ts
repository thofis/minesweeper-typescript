/**
 * Created by tom on 5/3/15.
 */

module org.bitbucket.thofis.minesweeper_typescript {

    export enum MineState {
        ONE_NEIGHBOR,
        TWO_NEIGHBORS,
        THREE_NEIGHBORS,
        FOUR_NEIGHBORS,
        FIVE_NEIGHBORS,
        SIX_NEIGHBORS,
        SEVEN_NEIGHBORS,
        EIGHT_NEIGHBORS,
        EMPTY,
        MINE
    }

    export enum Visibility {
        HIDDEN,
        VISIBLE
    }

    export enum FlagState {
        NO_FLAG,
        FLAGGED
    }

    export interface IBoard {
        getCell(col:number, row:number):Cell;
        getNrOfRows():number;
        getNrOfCols():number;
        click(col:number, row:number);
        stopGame();
        toggleFlag(col:number, row:number);
    }

    export class Controller implements IBoard {

        // 2d-array representing a board of cells: Cell[row][col]
        private board:Cell[][];

        constructor(private cols:number, private rows:number, private mines:number, private gameWonOrLostCallback:(boolean)=>void) {
            this.initBoard();
        }

        public getCell(col, row) {
            return this.board[row][col];
        }

        public getNrOfRows() {
            return this.rows;
        }

        public getNrOfCols() {
            return this.cols;
        }

        public click(col:number, row:number) {
            //TODO click handling
            this.board[row][col].setVisibility(Visibility.VISIBLE);
            if (this.board[row][col].getMineState() == MineState.MINE) {
                console.log("Game Over");
                this.gameWonOrLostCallback(false);
            } else {
                if (this.checkForWin() == true) {
                    this.gameWonOrLostCallback(true);
                }
                this.updateNeighborVisibility(col, row);
            }
        }

        public stopGame() {
            for (var r = 0; r < this.rows; r++) {
                for (var c = 0; c < this.cols; c++) {
                    this.board[r][c].setVisibility(Visibility.VISIBLE);
                }
            }
        }

        public toggleFlag(col:number, row:number) {
            this.board[row][col].toggleFlagState();
            if (this.board[row][col].getFlagState() == FlagState.FLAGGED) {
                if (this.checkForWin()) {
                    return this.gameWonOrLostCallback(true);
                }
            }
        }

        private checkForWin():boolean {
            // won if, all and only mine fields are flagged
            var flaggedMines = 0;
            var flaggedCells = 0;
            for (var r = 0; r < this.rows; r++) {
                for (var c = 0; c < this.cols; c++) {
                    var currentCell = this.board[r][c];
                    if (currentCell.getFlagState() == FlagState.FLAGGED) {
                        flaggedCells++;
                        if (currentCell.getMineState() == MineState.MINE) {
                            flaggedMines++;
                        }
                    }
                }
            }
            return flaggedCells == flaggedMines && flaggedMines == this.mines;
        }

        private initBoard() {
            // place mines randomly
            var minePlaces = [];
            for (var i = 0; i < this.cols * this.rows; i++) {
                minePlaces.push(i);
            }
            minePlaces = this.shuffleArray(minePlaces);
            minePlaces = minePlaces.slice(0, this.mines);

            for (var i = 0; i < minePlaces.length; i++) {
                console.log(minePlaces[i])
            }

            // setup board
            this.board = [];
            for (var r = 0; r < this.rows; r++) {
                this.board[r] = [];
                for (var c = 0; c < this.cols; c++) {
                    var mineState = MineState.EMPTY;
                    if (this.createMine(c, r, minePlaces)) {
                        mineState = MineState.MINE;
                    }
                    this.board[r][c] = new Cell(mineState, Visibility.HIDDEN, FlagState.NO_FLAG);
                }
            }

            // count neighbors
            for (var r = 0; r < this.rows; r++) {
                for (var c = 0; c < this.cols; c++) {
                    if (this.board[r][c].getMineState() == MineState.EMPTY) {
                        var neighbors = this.countNeighbors(c, r);
                        var newMineState:MineState;
                        switch (neighbors) {
                            case 1:
                                newMineState = MineState.ONE_NEIGHBOR;
                                break;
                            case 2:
                                newMineState = MineState.TWO_NEIGHBORS;
                                break;
                            case 3:
                                newMineState = MineState.THREE_NEIGHBORS;
                                break;
                            case 4:
                                newMineState = MineState.FOUR_NEIGHBORS;
                                break;
                            case 5:
                                newMineState = MineState.FIVE_NEIGHBORS;
                                break;
                            case 6:
                                newMineState = MineState.SIX_NEIGHBORS;
                                break;
                            case 7:
                                newMineState = MineState.SEVEN_NEIGHBORS;
                                break;
                            case 8:
                                newMineState = MineState.EIGHT_NEIGHBORS;
                                break;
                            default:
                                newMineState = MineState.EMPTY;
                        }
                        this.board[r][c].setMineState(newMineState);
                    }
                }
            }
        }

        private updateNeighborVisibility(col:number, row:number):void {
            for (var x = -1; x <= 1; x++) {
                for (var y = -1; y <= 1; y++) {
                    // check relevant neighbor cells
                    if (!(x == 0 && y == 0) &&
                        this.board[x + row] && this.board[x + row][y + col] &&
                        this.board[x + row][y + col].getVisibility() == Visibility.HIDDEN) {
                        var currentNeighbor = this.board[x + row][y + col];
                        if (currentNeighbor.getMineState()!=MineState.MINE) {
                            currentNeighbor.setVisibility(Visibility.VISIBLE);
                            if (currentNeighbor.getMineState() == MineState.EMPTY) {
                                this.updateNeighborVisibility(y+col, x+row);
                            }
                        }
                    }
                }
            }
        }

        private countNeighbors(col:number, row:number):number {
            var neighbors = 0;
            for (var x = -1; x <= 1; x++) {
                for (var y = -1; y <= 1; y++) {
                    if (this.board[x + row] && this.board[x + row][y + col] && this.board[x + row][y + col].getMineState() == MineState.MINE) {
                        neighbors++;
                    }
                }
            }
            return neighbors;
        }

        private shuffleArray<T>(o:Array<T>) {
            for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
            return o;
        }

        private createMine(col:number, row:number, minePlaces:number[]) {
            for (var i = 0; i < minePlaces.length; i++) {
                if (minePlaces[i] == col + row * this.cols) {
                    return true;
                }
            }
            return false;
        }


    }

    class Cell {

        constructor(private mineState:MineState, private visibility:Visibility, private flagState:FlagState) {

        }

        setMineState(mineState) {
            this.mineState = mineState;
        }

        getMineState() {
            return this.mineState;
        }

        getVisibility() {
            return this.visibility;
        }

        setVisibility(visibility) {
            this.visibility = visibility;
        }

        getFlagState() {
            return this.flagState;
        }

        setFlagState(flagState) {
            this.flagState = flagState;
        }

        toggleFlagState() {
            if (this.flagState == FlagState.FLAGGED) {
                this.flagState = FlagState.NO_FLAG;
            } else {
                this.flagState = FlagState.FLAGGED;
            }
        }


    }

}