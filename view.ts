/**
 * Created by tom on 5/3/15.
 */


/// <reference path="./typings/jquery/jquery.d.ts"/>
/// <reference path="./controller.ts"/>

module org.bitbucket.thofis.minesweeper_typescript {

    export class View {

        private parent:HTMLElement;
        private parentId: string;

        constructor(parentElementId:string, private board:IBoard) {
            this.parent = $(parentElementId).get(0);
            this.parentId = parentElementId;
            this.createBoard();
        }

        public stopGame() {
            this.updateBoard();
            this.removeAllEventHandlers();
        }

        public removeAllEventHandlers() {
            $(this.parentId+' *').off();
        }

        private createBoard() {
            $(this.parent).empty();
            var table = $('<table cellpadding="0" cellspacing="0"></table>');
            for (var r = 0; r < this.board.getNrOfRows(); r++) {
                var row = $('<tr></tr>');
                for (var c = 0; c < this.board.getNrOfCols(); c++) {
                    var col = $('<td class="cell"></td>');
                    col.append(this.createImg(c, r));
                    row.append(col);
                }
                table.append(row);
            }
            $(this.parent).append(table);
        }

        private createImageId(col:number, row:number):string {
            return 'c' + col + 'r' + row;
        }

        public updateBoard() {
            for (var r=0; r<this.board.getNrOfRows(); r++) {
                for (var c=0; c<this.board.getNrOfCols(); c++) {
                    var img = $('#'+this.createImageId(c,r));
                    this.setImageSource(this.board.getCell(c, r), img);
                }
            }
        }

        private createImg(col, row) {
            var img = $('<img id=' + this.createImageId(col, row) + ' class="cell">');
            var cell = this.board.getCell(col, row);
            this.setImageSource(cell, img);
            $(img).click(()=> {
                this.board.click(col, row);
                this.updateBoard();
            });
            // handler for right click => flag
            $(img).mousedown((e)=> {
                if (e.which == 3) {
                    this.board.toggleFlag(col, row);
                    this.updateBoard();
                }
            });

            return img;
        }

        private setImageSource(cell, img) {
            var imageName = '';
            //console.log(cell.getMineState().toString())
            if (cell.getVisibility() == Visibility.HIDDEN) {
                if (cell.getFlagState() == FlagState.FLAGGED) {
                    imageName = 'flag';
                } else {
                    imageName = 'hidden';
                }
            } else {
                switch (cell.getMineState()) {
                    case MineState.ONE_NEIGHBOR:
                        imageName = '1';
                        break;
                    case MineState.TWO_NEIGHBORS:
                        imageName = '2';
                        break;
                    case MineState.THREE_NEIGHBORS:
                        imageName = '3';
                        break;
                    case MineState.FOUR_NEIGHBORS:
                        imageName = '4';
                        break;
                    case MineState.FIVE_NEIGHBORS:
                        imageName = '5';
                        break;
                    case MineState.SIX_NEIGHBORS:
                        imageName = '6';
                        break;
                    case MineState.SEVEN_NEIGHBORS:
                        imageName = '7';
                        break;
                    case MineState.EIGHT_NEIGHBORS:
                        imageName = '8';
                        break;
                    case MineState.MINE:
                        imageName = 'mine';
                        break;
                    default :
                        imageName = 'empty';
                        break;
                }
            }

            img.attr('src', 'images/' + imageName + '.png');
        }

    }

}