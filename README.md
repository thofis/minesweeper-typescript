# Minesweeper-Typescript #

This is a simple implementation of the Minesweeper game implemented in Typescript.
It served me as an exercise for learning Typescript.

### Setup ###

* install bower (if not already installed)
* type: bower install (to download the jQuery distribution that is used for the UI stuff)

### Compilation ###

* install tsd (typescript compiler, if not already installed)
* type: tsd *.ts (to generate the js files)